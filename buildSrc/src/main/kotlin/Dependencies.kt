private object Versions {
    const val kotlin = "1.3.72"
    const val buildTools = "4.0.2"
    const val androidxCore = "1.3.2"
    const val appCompat = "1.2.0"
    const val constraintLayout = "2.0.4"
    const val junit = "4.13"
}

object Plugins {
    const val buildTools = "com.android.tools.build:gradle:${Versions.buildTools}"
    const val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
}

object Libs {
    // Setup
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"
    const val androidxCore = "androidx.core:core-ktx:${Versions.androidxCore}"

    // UI
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"

    // Test
    const val junit = "junit:junit:${Versions.junit}"
}