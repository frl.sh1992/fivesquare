This is a sample of Foursquare containing two pages: list of venues, and detailed information for each one. 
- The data is based on user location, so if user moves (up to 10 meters) the data will gets updated.
- Also it's an offline first application that means when the user does't have access to the Intenet, the latest cached data will shown.

This project is based on MVVM as a architecture design pattern, coroutines for thread management, koin for DI, and Room is used for caching data, also it contains unit tests to make sure everything goes well.
